(require 'org)
(org-babel-load-file
 (expand-file-name "settings.org"
                   user-emacs-directory))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auth-source-save-behavior nil)
 '(delete-selection-mode t)
 '(dired-auto-revert-buffer t)
 '(ede-project-directories '("/tmp/test/include" "/tmp/test/src" "/tmp/test"))
 '(eshell-scroll-to-bottom-on-input 'all)
 '(gc-cons-threshold 100000000)
 '(gdb-many-windows t)
 '(global-auto-revert-mode t)
 '(global-auto-revert-non-file-buffers t)
 '(lsp-log-io nil)
 '(org-agenda-files '("~/Nextcloud/todo.org"))
 '(org-babel-load-languages '((C . t) (emacs-lisp . t)))
 '(package-selected-packages
   '(platformio-mode arduino-cli-mode arduino-mode company-arduino gnu-elpa-keyring-update eat pkg-info flycheck-grammalecte lsp-treemacs tabspaces web-mode org-msg elfeed elfeed-org elfeed-score yasnippet-snippets slime which-key s lsp-mode iedit magit lsp-ui company flycheck use-package yasnippet))
 '(scroll-bar-mode nil)
 '(server-mode nil)
 '(tabspaces-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "unknown" :slant normal :weight normal :height 87 :width normal))))
 '(lsp-ui-doc-background ((t (:background nil))))
 '(lsp-ui-doc-header ((t (:inherit (font-lock-string-face italic))))))
